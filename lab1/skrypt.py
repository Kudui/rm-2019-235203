import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from numpy.random import seed
from numpy.random import rand

delta = 10 # wspolczynnik dokladnosci mapowania ( na kazda jednostke 10 pozycji )

x_size = 10 # wymiar poziomy pola
y_size = 10 # wymiar pionowy pola
obst_num=20
start_point=[-10,-3]    # pozycja startu
finish_point=[10,3]     # pozycja celu
obst_vect = [[0, 0]] *obst_num #[(1, 2), (1.5, 2.3), (0,1), (1,0)] # pozycje przeszkod
nb= [9999]*8
snb=nb
koh=7
limiter=100
# randomizer
temp = 0
while temp < obst_num:
    a = rand(2)
    obst_vect[temp] = [round((a[0]*20 -10),2),round((a[1]*20 -10),2)]
    temp += 1
a=rand(1)
start_point[1]=round((a[0]*20 -10),1)
a=rand(1)
finish_point[1]=round((a[0]*20 -10),1)
road = [start_point]




kp=10
ko=10
d0=20




x = y = np.arange(-10.0, 10.0, 1/delta)
X, Y = np.meshgrid(x, y)
Z = X.copy()
rob_local=start_point  # zmienna przedstawiajaca aktualna pozycje robota
i=len(Y)
j=len(X)
Uw=X.copy()    # potencjal wypadkowy
Fp=X.copy()    # potencjal przyciagajacy

SVoi=X.copy()
Fw=X.copy()
j=0
while j<len(Y):  # rozpatrywany wiersz chwilowo nie kojarzę lepszej alternatywy do iterowania po wyrazie
    
    i=0
    while i<len(X):  #rozpatrywana kolumna w rzędzie
        #liczenie wartości zachęcającej robota do przebywania w tym miejscu
        sum = 0
        V = ( x[i] - finish_point[0], y[j] - finish_point[1] )  #wektor między aktualnym badanym punktem a końcowym
        
        norma=np.sqrt ( V[0] * V[0] + V[1] * V[1] )             #liczenie normy(kto by się spodziewał)
        Fp[i][j] = (kp) * ( norma)                              #wyliczanie siły przyciągającej dla danego punktu i wrzucenie wartosci do "macierzy przyciągania"
        
        #liczenie wartości zniechęcającej robota do przebywania w tym miejscu
        for voi in obst_vect:                                   #dla każdej przeszkody
            V2 = ( x[i] - voi[0], y[j] - voi[1] )               #odległość danej przeszkody od aktualnie badanego punktu
            norma=np.sqrt ( V2[0] * V2[0] + V2[1] * V2[1] )     #liczenie normy dla wskazanego wektora
            if ( norma ) <= d0:                                 #jeżeli norma nie jest zbyt duża(odległość od przeszkody nie jest na tyle duża aby ignorować jej wpływ)
                sum += ( ( -ko ) * (( 1 / norma ) -( 1 / d0) )*( 1 / ( norma ** 2 ) )   )#dodaj do negatywnego współczynnika wartość wg wzoru z pdfu
        #limiter dla czytelności grafu(bo bez niego jest cienko)
        if sum < -limiter :                                     #jezeli negatywny współczynnik jest cutes za duży(wartość rośmie eksponencjalnie z odwrtonością odległości)
            sum= -limiter                                       #to go kurczaczek zmniejsz do naszego limitu czytelności
        #transponowanie macierzy
        SVoi[i][j] = sum                                        #wrzucenie wartości negatywnego współczynnika do "macierzy odpychania"
        Fw[i][j] =  Fp[i][j] - SVoi[i][j]                       #wartość tego punktu w macieży wypadkowej równa jest wartości współczynnika pozytywnego minus negatywnego
        i += 1                                                  #to jest do iterowania bo nie umiem w pythona
    j += 1                                                      #kto by sie spodziewał tu jest tak samo

Z = Fw.T                                                        #tak naprawdę to nie wiem gdzie jest błąd ale nie powinienem tutaj potrzebować tego transponowania

temp=0

#szukanie ścieżki
#ściąga z nrów somsiadów
# 3 5 8
# 2 R 7
# 1 4 6

print("start", start_point,"finisz",finish_point)
rx=int((rob_local[0]+10)*delta)                                 #współrzędne aktualnej pozycji dla algorytmu wyznaczania ścieżki
ry=int((rob_local[1]+10)*delta)                                 #
while [(rx/delta)-x_size,(ry/delta)-y_size] !=finish_point:
    nb=snb


    if rx == 0:
        if ry==0:
            nb[4]=Fw[rx][ry+1]
            nb[6]=Fw[rx+1][ry]
            nb[7]=Fw[rx+1][ry+1]
        elif ry==2*y_size*delta-1:                        
            nb[3]=Fw[rx][ry-1]
            nb[5]=Fw[rx+1][ry-1]
            nb[6]=Fw[rx+1][ry]
        else:
            nb[3]=Fw[rx][ry-1]
            nb[4]=Fw[rx][ry+1]
            nb[5]=Fw[rx+1][ry-1]
            nb[6]=Fw[rx+1][ry]
            nb[7]=Fw[rx+1][ry+1]
    elif rx==2*x_size*delta-1:
        if ry==0:
            nb[4]=Fw[rx][ry+1]
            nb[1]=Fw[rx-1][ry]
            nb[2]=Fw[rx-1][ry+1]
        elif ry==2*y_size*delta-1:                        
            nb[3]=Fw[rx][ry-1]
            nb[0]=Fw[rx-1][ry-1]
            nb[1]=Fw[rx-1][ry]
        else:
            nb[3]=Fw[rx][ry-1]
            nb[4]=Fw[rx][ry+1]
            nb[0]=Fw[rx-1][ry-1]
            nb[1]=Fw[rx-1][ry]
            nb[2]=Fw[rx-1][ry+1]
    else:
        if ry==0:
            nb[1]=Fw[rx-1][ry]
            nb[2]=Fw[rx-1][ry+1]
            nb[4]=Fw[rx][ry+1]
            nb[6]=Fw[rx+1][ry]
            nb[7]=Fw[rx+1][ry+1]
        elif ry==2*y_size*delta-1:    
            nb[0]=Fw[rx-1][ry-1]
            nb[1]=Fw[rx-1][ry]
            nb[3]=Fw[rx][ry-1]
            nb[5]=Fw[rx+1][ry-1]
            nb[6]=Fw[rx+1][ry]
        else:
            nb[0]=Fw[rx-1][ry-1]
            nb[1]=Fw[rx-1][ry]
            nb[2]=Fw[rx-1][ry+1]
            nb[3]=Fw[rx][ry-1]
            nb[4]=Fw[rx][ry+1]
            nb[5]=Fw[rx+1][ry-1]
            nb[6]=Fw[rx+1][ry]
            nb[7]=Fw[rx+1][ry+1]

    

    #szukanie "króla wzgórza"
    n=0
    while n<8:
        if nb[n]<nb[koh]:
            koh=n
        n+=1
    #print (koh)

    #obranie nowej pozycji
    if   koh==0:
        nx=rx-1
        ny=ry-1
    elif koh==1:
        nx=rx-1
        ny=ry
    elif koh==2:
        nx=rx-1
        ny=ry+1
    elif koh==3:
        nx=rx
        ny=ry-1
    elif koh==4:
        nx=rx
        ny=ry+1
    elif koh==5:
        nx=rx+1
        ny=ry-1
    elif koh==6:
        nx=rx+1
        ny=ry
    elif koh==7:
        nx=rx+1
        ny=ry+1
        
    rx=nx
    ry=ny

    #print(nx,ny)
    road.append([(rx/delta)-x_size,(ry/delta)-y_size])
    #print ([rx,ry],finish_point)
    temp+=1



fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])#



plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for point in road:
    if point != finish_point:
        plt.plot(point[0], point[1], "or", color='cyan',markersize=3)

#for obstacle in obst_vect:
#    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
#input("type any key")


